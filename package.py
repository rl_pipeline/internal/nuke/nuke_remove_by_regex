name = "nuke_remove_by_regex"

version = "0.1.0"

description = "Remove or keep channels with the given regular expression."

authors = ["rickylinton"]

with scope("config") as config_data:
    config_data.release_packages_path = "${REZ_EXTERNAL_RELEASE_PATH}"

build_requires = ["cmake-3.12<4"]

variants = [["gcc-6.3", "nuke-10.5v4"]]


def commands():
    env.NUKE_PATH.append("{root}/lib")
    env.NUKE_PATH.append("{root}/python")

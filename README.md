# [RemoveByRegex](https://gitlab.com/rl_pipeline/internal/nuke/nuke_remove_by_regex)

### Overview

A simple Nuke c++ plugin to remove or keep channels of the input from the given
regular expression, built and released in [rez](https://github.com/nerdvegas/rez) environment.

![RemoveByRegex](images/nuke_remove_by_regex.jpg)

### Quick Demo
Right click > Open image in new tab (To see full resolution image)
<img src="images/quick_demo.gif" alt="quick_demo" width="2558"/>

### Disclaimer
This was built and tested under:
* GNU Make 4.1
* Ubuntu 18.04.3 LTS
* cmake-3.14.7
* gcc-6.3.0
* nuke-10.5.v4
* rez-2.54.0

### Prerequisites
* cmake-3.12+
* gcc-6.3+
* nuke
* [rez package manager](https://github.com/nerdvegas/rez)

### How to Build or Release the Plugin
```bash
git clone https://gitlab.com/rl_pipeline/internal/nuke/nuke_remove_by_regex.git
cd nuke_remove_by_regex
git checkout tags/<tagname>
rez-build -i # This is to build into your local package directory.
# OR
rez release -m "Released" # This is to release into your release package directory. 
# Note in the package.py, you need to set REZ_EXTERNAL_RELEASE_PATH environment variable for the release directory.
```
#include "RemoveByRegex.hpp"

const char *const RemoveByRegex::RCLASS = "RemoveByRegex";

const char *const RemoveByRegex::OPERATIONS[] = {"remove", "keep", nullptr};

const char *RemoveByRegex::Class() const {return RCLASS;}

const char *RemoveByRegex::node_help() const {return "Remove or keep channels from the input with the given regular expression.";}

void RemoveByRegex::_validate(bool for_real)
{
    copy_info();

    DD::Image::ChannelSet matchedChannels;
    std::regex regex(regexInput);
    foreach (chn, info_.channels())
    {
        const char *layerName = DD::Image::getLayerName(chn);
        if (std::regex_match(layerName, regex))
            matchedChannels.insert(chn);
    }

    // keep
    if (operation)
    {
        info_.channels() &= (matchedChannels);
        set_out_channels(info_.channels());
    }
    else
    { // remove
        info_.turn_off(matchedChannels);
        set_out_channels(matchedChannels);
    }
}

int RemoveByRegex::knob_changed(DD::Image::Knob *k)
{
    std::string knobName = k->name();
    if (knobName.compare("regex") == 0 || knobName.compare("operation") == 0)
    {
        asapUpdate();
        return true;
    }
    return Iop::knob_changed(k);
}

void RemoveByRegex::knobs(DD::Image::Knob_Callback f)
{
    DD::Image::String_knob(f, &regexInput, "regex");
    DD::Image::Enumeration_knob(f, &operation, OPERATIONS, "operation");
}

static DD::Image::Iop *build(Node *node) { return new RemoveByRegex(node); }

DD::Image::NoIop::Description RemoveByRegex::d(RCLASS, "Color/RemoveByRegex", build);
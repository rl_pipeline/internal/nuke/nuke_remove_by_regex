// Copyright (c) 2021 Ricky Linton.  All Rights Reserved.

/* Remove or keep channels from the input with the given regular expression. */
#pragma once

#include <regex>

#include "DDImage/NoIop.h"
#include "DDImage/Knobs.h"

class RemoveByRegex : public DD::Image::NoIop
{
    // methods
public:
    RemoveByRegex() = delete;
    explicit RemoveByRegex(Node *node) : DD::Image::NoIop(node),
                                channels(DD::Image::Mask_All),
                                operation(0) {}
    void knobs(DD::Image::Knob_Callback) override;
    int knob_changed(DD::Image::Knob *) override;
    const char *Class() const override;
    const char *node_help() const override;
    static DD::Image::NoIop::Description d;

    // methods
private:
    void _validate(bool) override;

    // variables
private:
    int operation; // 0 = remove, 1 == keep
    std::string regexInput;
    DD::Image::ChannelSet channels;
    static const char *const OPERATIONS[];
    static const char *const RCLASS;
};